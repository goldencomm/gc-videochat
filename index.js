let express = require('express');

//Load module to read env file
require('dotenv').config();

let redis = require("redis");
let redisClient = redis.createClient({host: process.env.REDIS_HOST || 'localhost'});

let bodyParser = require("body-parser");
let router = express.Router();
const path = require('path');

let AuthorizationManager = require('./classes/Authorization.js');
let Authorization = new AuthorizationManager();

//Open Talk
let OpenTalk = require('./classes/OpenTalk.js');
let app = express();
let OTApiKey = process.env.OT_API_KEY;
let OTApiSecret = process.env.OT_API_SECRECT;

//Cors
let cors = require('cors');
app.use(cors({
    origin: '*'
}));

app.use(bodyParser.json());

//Discord
const DiscordManager = require('./classes/Discord.js')
const DiscordModel = new DiscordManager();

/**
 * ROUTE API
 */
app.get('/video-chat/v1/generate-new-chat', async function (req, res) {

    //Validate access to app
    let validate = Authorization.verifyToken(req, res);
    if (!validate) {
        res.sendStatus(403);
        return;
    }

    async function generateNewChat(){
        let OT = new OpenTalk(OTApiKey, OTApiSecret);
        let session = await OT.generateSession();

        let videoID = await OT.setSessionToRedis(session);

        await DiscordModel.authorize();
        await DiscordModel.sendMessage(videoID);

        return {
            'videoID': videoID
        };
    }

    let chatData = await generateNewChat();

    res.send(chatData);
});

app.post('/video-chat/v1/discord-send-form-message', async function (req, res) {

    //Validate access to app
    let validate = Authorization.verifyToken(req, res);
    if (!validate) {
        res.sendStatus(403);
        return;
    }

    if (typeof req.body.data !== 'undefined') {
        const data = req.body.data;

        async function sendMessage() {
            await DiscordModel.authorize();
            await DiscordModel.sendFormMessage(data);
        }
        await sendMessage();

        res.sendStatus(201);
    }else{
        res.sendStatus(400);
    }
});

app.get('/video-chat/v1/generate-token', function (req, res) {
    //Validate access to app
    let validate = Authorization.verifyToken(req, res);
    if (!validate) {
        res.sendStatus(403);
        return;
    }

    let OT = new OpenTalk(OTApiKey, OTApiSecret);
    let token = OT.generateToken(req.query.sessionID, req.query.role, {});
    res.send(token);
});

/**
 * Get from Redis TokBox Session ID based on short URL
 * @param vid (string) Short URL generated in /video-chat/v1/set-session
 * @return 500 If some error | JSON with TokBox Session ID
 */
app.get('/video-chat/v1/get-session', function(req, res) {
    //Validate access to app
    let validate = Authorization.verifyToken(req, res);
    if (!validate) {
        res.sendStatus(403);
        return;
    }

    redisClient.on("error", function(error) {
        console.error(error);
        res.sendStatus(500);
    });

    if (typeof req.query.vid !== 'undefined') {
        let videoID = req.query.vid;

        redisClient.get(videoID, function (err, reply) {
            if(err != null){
                console.log(err);
                res.sendStatus(500);
            }else {
                res.send(reply);
            }
        });
    }else{
        res.sendStatus(500);
    }
});

app.listen(80, function () {
    console.log('Example app listening on port 80!');
});
