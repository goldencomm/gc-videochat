# Production

## Docker Repository
Docker repository: https://docker.gscadmin.com:5000/v2/_catalog

## Build docker image and push to docker repo

1. Create docker image: 

`docker build -t docker.gscadmin.com:5000/gc-video-chat .`

2. Login to docker repository:

`docker login docker.gscadmin.com:5000`

Login and password available on LastPass

3. Push docker image to the repository

`docker push docker.gscadmin.com:5000/gc-video-chat`

*Do not forget send to server .env file


## Update docker image and push to docker repo

1. Create new docker image:

`docker build -t docker.gscadmin.com:5000/gc-video-chat:<version> .`

**version** - Version ID (actual version available in package.json file) new image e.g.:

`docker build -t docker.gscadmin.com:5000/gc-video-chat:v1.1 .`

2. Login to docker repository:

`docker login docker.gscadmin.com:5000`

Login and password available on LastPass

3. Push docker image to the repository

`docker push docker.gscadmin.com:5000/gc-video-chat`

*Do not forget send to server .env file if you made some changes

---

#Development

How to install on development env project.

1. Install npm package `npm install`
2. Run *Node* server `npm run start`

##Example page

Example page will be available on url: `http://localhost/example/example-1/customer` and code is available
here: `public/template/example-1/customer.html`

To generate new chat session use API call: `/video-chat/v1/generate-new-chat`
