const Discord = require("discord.js");

module.exports = class DiscordManager {

    constructor() {
        this.client = new Discord.Client();
    }

    authorize() {
        return new Promise(((resolve, reject) => {
            if (this.client.user != null) {
                resolve();
            } else {
                this.client.on('ready', () => {
                    resolve();
                });
                this.client.login(process.env.DISCORD_TOKEN);
            }
        }));
    }

    sendMessage(videoID) {
        const DISCORD_CHANNEL_ID_VIDEO_CHAT = process.env.DISCORD_CHANNEL_ID_VIDEO_CHAT;

        this.client.channels.fetch(DISCORD_CHANNEL_ID_VIDEO_CHAT).then(channel => {

            const messageEmbed = new Discord.MessageEmbed()
                .setColor('#FF0000')
                .setTitle('New Video Chat')
                .setDescription('Chat ID: ' + videoID)
                .setURL(process.env.DOMAIN + process.env.URL_MODERATOR + '?vid=' + videoID)
                .setImage('https://cdn.pixabay.com/photo/2017/05/01/14/59/call-center-2275745_960_720.jpg')
                .setTimestamp()
            ;

            channel.send(messageEmbed)
        });
    }

    sendFormMessage(data) {
        const DISCORD_CHANNEL_ID_VIDEO_CHAT = process.env.DISCORD_CHANNEL_ID_VIDEO_CHAT;

        this.client.channels.fetch(DISCORD_CHANNEL_ID_VIDEO_CHAT).then(channel => {

            const messageEmbed = new Discord.MessageEmbed()
                .setColor('#FF0000')
                .setTitle('Video Chat Customer Data')
                .setDescription('Chat ID: ' + data.vid)
                .addField('First Name', data.firstName)
                .addField('Email', data.email)
                .addField('Phone', data.phone)
                .setURL(process.env.DOMAIN + process.env.URL_MODERATOR + '?vid=' + data.vid)
                .setTimestamp()
            ;

            channel.send(messageEmbed)
        });
    }
}
