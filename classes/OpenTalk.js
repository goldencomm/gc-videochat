let OpenTok = require('opentok');
let redis = require("redis");

module.exports = class OpenTalk {

    constructor(ApiKey, ApiSecret) {
        this.ApiKey = ApiKey;
        this.ApiSecret = ApiSecret;
        this.ot = new OpenTok(ApiKey, ApiSecret);
    }

    /**
     * Generate a basic session. Or you could use an existing session ID.
     */
    generateSession() {
        let sessionId;

        return new Promise(((resolve, reject) => {
            this.ot.createSession({}, (error, session) => {

                if (error) {
                    reject(error)
                } else {
                    sessionId = session.sessionId;
                    resolve({
                        'sessionId': sessionId
                    });
                }
            });
        }));
    }

    /**
     * Generate Token
     * @param sessionID
     * @param role
     * @param data
     * @returns {*}
     */
    generateToken(sessionID, role, data) {
        let tokenOptions = {};
        tokenOptions.role = role;
        tokenOptions.data = "username=bob3";
        return this.ot.generateToken(sessionID, tokenOptions);
    }

    setSessionToRedis(session) {
        return new Promise(((resolve, reject) => {
            let redisClient = redis.createClient({host: process.env.REDIS_HOST || 'localhost'});

            redisClient.on("error", function (error) {
                console.error(error);
                reject(error)
            });

            if (typeof session.sessionId !== 'undefined') {
                let OpenTokSession = session.sessionId;
                let videoID = (Math.random() + 1).toString(36).substring(7);

                redisClient.set(videoID, JSON.stringify({'sessionID': OpenTokSession}), function (err, reply) {
                    resolve(videoID);
                });
            } else {
                console.error('req.query.s is undefined');
                reject('req.query.s is undefined')
            }
        }));
    }
}
