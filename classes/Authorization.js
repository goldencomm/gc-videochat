module.exports = class AuthorizationManager {
    verifyToken(req, res) {
        const bearerHeader = req.headers['authorization'];
        if (typeof bearerHeader !== 'undefined') {
            const bearer = bearerHeader.split(' ');
            const bearerToken = bearer[1];

            if (bearerToken === process.env.BEARER_TOKEN) {
                return true;
            }
        }
        return false;
    }
}
