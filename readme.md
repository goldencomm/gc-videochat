# Video Chat API
Video Chat API to communicate between customer and customer support in company. 
This API generates Video Talk Session and send link to this conversation to the Discord Channel.

## What technology are we used?
- node.js
- node server Express
  
## What API we use?
- OpenTalk API
- Discord API

## Example of usage
`/example/example-1/customer` - on this page you can push button to generate session/video chat

# Video Chat API

## Generate New Chat

- `method`: GET
- `URL`: video-chat/v1/generate-new-chat
- `Authorization`: Bearer Token

**Response**

```json
{
    "sessionId": "session-id-strong-from-api"
}
```

## Generate Token

- `method`: GET
- `URL`: video-chat/v1/generate-token
- `param`: 
  - `sessionID` (string): sessionID
  - `role` (string): {subscriber|publisher|moderator}
- `authorization` (string): Bearer Token

**Response**

```
token-token-token-token
```

## Generate Token

- `method`: GET
- `URL`: video-chat/v1/get-session
- `param`:
  - `vid` (string): vid
- `authorization` (string): Bearer Token

**Response**

```json
{
  "sessionID":"2_MX40NzI4NjE3NH5-MTYyOTk3MjkzNDI4Nn5XM2RuZlNuYWttdDgxM3JhaHFhMTNPYWx-UH4"
}
```

## Discord - send message witch customer data

- `method`: POST
- `URL`: video-chat/v1/discord-send-form-message
- `authorization` (string): Bearer Token
- `type`: JSON
- `body`:
```json
{
  "data": {
    "vid" : "hxcry",
    "firstName" : "Jan",
    "email": "test@goldencomm.com",
    "phone": "666666666"
  }
}
```
*All fields are required!


**Response**

- 201 - Created new message
- 400 - Issues witch send message
- 403 - No access
