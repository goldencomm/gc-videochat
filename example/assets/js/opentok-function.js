
function urlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
        .exec(window.location.search);

    return (results !== null) ? results[1] || 0 : false;
}

function initializeSession(apiKey, sessionId, token) {
    let session = OT.initSession(apiKey, sessionId);

    // Subscribe to a newly created stream
    session.on('streamCreated', function (event) {
        session.subscribe(event.stream, 'subscriber', {
            insertMode: 'append',
            width: '100%',
            height: '100%'
        }, handleError);

        $('#videos #subscriber .preloader').hide();
    });

    // Create a publisher
    let publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
    }, handleError);

    // Connect to the session
    session.connect(token, function (error) {
        // If the connection is successful, initialize a publisher and publish to the session
        if (error) {
            handleError(error);
        } else {
            session.publish(publisher, handleError);
        }
    });
}

function handleError(error) {
    if (error) {
        alert(error.message);
    }
}

/**
 * Open Talk Helper
 * @type {OpenTalkHelper}
 */
class OpenTalkHelper {
    session = 'test session';


    /**
     * @param apiKey
     * @param sessionID
     * @param token
     */
    constructor(apiKey, sessionID) {
        this.apiKey = apiKey;
        this.sessionID = sessionID;
        this.token = '';
    }

    getSession(){
        return this.session;
    }

    getPublisher(){
        return this.publisher;
    }

    setToken(token){
        this.token = token;
    }

    initSession(){
        this.session = OT.initSession(this.apiKey, this.sessionID);
        this.subscribeToExistingSession(this.session);
        this.publisher = this.createPublisher();
        this.connectToSession(this.session, this.publisher);
    }

    subscribeToExistingSession(session){
        // Subscribe to a newly created stream
        session.on('streamCreated', function (event) {
            session.subscribe(event.stream, 'subscriber', {
                insertMode: 'append',
                width: '100%',
                height: '100%'
            }, this.handleError);

            $('#videos #subscriber .preloader').hide();
        });
    }

    createPublisher(){
        return this.publisher = OT.initPublisher('publisher', {
            insertMode: 'append',
            width: '100%',
            height: '100%'
        }, this.handleError);
    }

    connectToSession(session, publisher){
        // Connect to the session
        session.connect(this.token, (error) =>  {
            // If the connection is successful, initialize a publisher and publish to the session
            if (error) {
                this.handleError(error);
            } else {
                session.publish(publisher, this.handleError(error));
            }
        });
    }

    handleError(error) {
        if (error) {
            alert(error.message);
        }
    }
}
